FROM openjdk
WORKDIR /usr/app
COPY ./target/spring-petclinic-*.war /usr/app/
EXPOSE 8080
CMD java -jar /usr/app/spring-petclinic-*.war
