pipeline {
    agent any 
    tools {
        maven 'maven'
    } 
    
    environment {
        APP_NAME = 'java-maven-app'
    }

    stages {
        stage('Compile Code'){
            steps{
                echo "compiling application codes"
                sh 'mvn clean compile'
            }
        }

        stage('test code'){
            steps{
                echo "test application codes"
                sh 'mvn test'
            }
        }

        stage ("Incrementing version") {
            steps {
                script {
                    echo "Incrementing version"
                    sh "mvn build-helper:parse-version versions:set \
                    -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                    versions:commit"
                    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                    def version = matcher[0][1]
                    env.IMAGE = "$version-$BUILD_NUMBER"
                }
            }
        }

        stage ("commit new version to github") {
            steps {
                script {
                    echo "committing new version to github"
                    withCredentials([usernamePassword(credentialsId:'github-id', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
                        sh 'git config --global user.email "jenkins@example.com"' 
                        sh 'git config --global user.name "jenkins"'

                        sh "git status"
                        sh "git branch"

                        sh "git remote set-url origin https://$USER:$PASS@github.com/davacho/mvn-app.git"
                        sh "git add ."
                        sh 'git commit -m "first jenkins commit"'
                        sh "git push origin HEAD:main"
                    }

                }
            }
        }

        stage('Build Code'){
            steps{
                echo "packaging the application"
                sh 'mvn clean package -Dmaven.test.skip'
            }
        }

        stage ("Building Docker Image and pushing to AWS ECR") {
            steps {
                script {
                    echo "building a docker image"
                    withCredentials([usernamePassword(credentialsId:'docker-hub-id', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
                        sh "docker build -t davidachoja/node-app:$IMAGE ."
                        sh "echo $PASS | docker login -u $USER --password-stdin"
                        sh "docker push davidachoja/node-app:$IMAGE"
                    }
                }
            }
        }

        stage('Trigger Playbooks') {
            steps { 
                script {
                    def ansibleSERVER         = 'ubuntu@10.0.4.239'
                    def ansibleInstallation   = 'ansible-playbook playbooks/installation.yaml'
                    def ansibleMaster         = 'ansible-playbook playbooks/master.yaml'
                    def ansibleWorker         = 'ansible-playbook playbooks/worker.yaml'
                    def ansibleJenkins        = 'ansible-playbook playbooks/jenkins.yaml'
                    def ansibleMonitoring     = 'ansible-playbook playbooks/monitoring.yaml'
                    sshagent(['ansible-key']) {
                        sh "ssh -o StrictHostKeyChecking=no ${ansibleSERVER} ${ansibleInstallation}"
                        sh "ssh -o StrictHostKeyChecking=no ${ansibleSERVER} ${ansibleMaster}"
                        sh "ssh -o StrictHostKeyChecking=no ${ansibleSERVER} ${ansibleWorker}"
                        sh "ssh -o StrictHostKeyChecking=no ${ansibleSERVER} ${ansibleJenkins}"
                        sh "ssh -o StrictHostKeyChecking=no ${ansibleSERVER} ${ansibleMonitoring}"
                    }
                }
            }
        }    

        stage('Deploy into Stage namespace') {
            steps { 
                script {
                    echo "deploying java-maven app into stage namespace in kubernetes cluster"
                    sh 'envsubst < stage.yaml | kubectl apply -f -'
                }
            }
        }

        stage ('Deploy To Prod namespace'){
            input{
                message "Do you want to proceed with production deployment?"
            }
            steps { 
                script {
                    echo "deploying java-maven app into prod namespace in kubernetes cluster"
                    sh 'envsubst < prod.yaml | kubectl apply -f -'
                }
            }
        }

    }
}